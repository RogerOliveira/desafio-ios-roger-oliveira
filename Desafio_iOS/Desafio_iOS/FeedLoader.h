//
//  FeedLoader.h
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/24/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "FeedLoaderDelegate.h"
#import "FeedModel.h"

extern NSString* const BASE_URL;
extern NSString* const URL_PATH;

@interface FeedLoader : AFHTTPSessionManager

+ (FeedLoader*) getInstance;
- (void) reload;
- (void) loadNext;

@property (nonatomic, assign) id<FeedLoaderDelegate> delegate;
@property (nonatomic, readwrite) int totalPages;
@property (nonatomic, readwrite) int currentPage;
@property (nonatomic, strong) NSMutableArray* shots;


@end

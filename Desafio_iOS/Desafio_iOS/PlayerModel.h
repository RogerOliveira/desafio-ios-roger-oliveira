//
//  PlayerModel.h
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/24/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"

@interface PlayerModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* photoURL;

@end

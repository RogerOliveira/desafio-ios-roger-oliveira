//
//  FeedLoader.m
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/24/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import "FeedLoader.h"

NSString * const BASE_URL = @"https://api.dribbble.com";
NSString * const URL_PATH = @"shots/popular?page=";

@implementation FeedLoader
@synthesize delegate, totalPages, currentPage, shots;

+ (FeedLoader*) getInstance {
    static FeedLoader *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    });
    return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    self.totalPages = -1;
    self.currentPage = 0;
    self.shots = [[NSMutableArray alloc] init];
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    return self;
}

- (void) reload {
    self.totalPages = -1;
    self.currentPage = 0;
    self.shots = [[NSMutableArray alloc] init];
    [self loadNext];
}

- (void) loadNext {
    
    self.currentPage++;
    
    NSLog(@"Loading page: %i", self.currentPage);
    
    NSString* path = [NSString stringWithFormat:@"%@%i", URL_PATH, self.currentPage];
    
    [self GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [self parseResults:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if(delegate)
            [delegate feedLoaderFailed];
    }];
}

- (void) parseResults: (id) responseObject {
    if(!delegate)
        return;
    
    NSError *error;
   
    FeedModel* result = [MTLJSONAdapter modelOfClass:[FeedModel class] fromJSONDictionary:responseObject error:&error];
    if (error) {
        [delegate feedLoaderFailed];
        return;
    }
        
    self.totalPages = result.totalPages;
    [self.shots addObjectsFromArray:[result shots]];
    
    [delegate feedLoaded:self.shots totalPages:self.totalPages currentPage:self.currentPage];
}

@end

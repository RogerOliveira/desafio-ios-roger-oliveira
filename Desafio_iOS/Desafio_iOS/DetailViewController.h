//
//  DetailViewController.h
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/26/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DetailViewControllerDelegate.h"
#import "ShotModel.h"


@interface DetailViewController : UIViewController

- (IBAction) backPressed;
- (void) setupData:(ShotModel*) shot;
@property (nonatomic, assign) id<DetailViewControllerDelegate> delegate;
@property (nonatomic, strong) IBOutlet UIImageView* shot_image;
@property (nonatomic, strong) IBOutlet UILabel* shot_title;
@property (nonatomic, strong) IBOutlet UIImageView* user_image;
@property (nonatomic, strong) IBOutlet UILabel* user_name;
@property (nonatomic, strong) IBOutlet UIWebView* shot_description;
@property (nonatomic, strong) ShotModel* currentShot;
@end

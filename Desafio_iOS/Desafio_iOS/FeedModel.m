//
//  FeedModel.m
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/25/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import "FeedModel.h"

@implementation FeedModel
@synthesize totalPages, shots;

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"totalPages" : @"pages",
             @"shots" : @"shots"
             };
}

+ (NSValueTransformer *)shotsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[ShotModel class]];
}

@end

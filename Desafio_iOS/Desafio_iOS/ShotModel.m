//
//  ShotModel.m
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/24/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import "ShotModel.h"

@implementation ShotModel
@synthesize title, shotDescription, image_url, player;

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"title" : @"title",
             @"shotDescription" : @"description",
             @"image_url" : @"image_url",
             @"player" : @"player"
             };
}

+ (NSValueTransformer *)playerJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[PlayerModel class]];
            //transformerForModelPropertiesOfClass:[PlayerModel class]];
}


@end

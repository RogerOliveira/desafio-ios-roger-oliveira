//
//  DetailViewControllerDelegate.h
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/26/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DetailViewControllerDelegate <NSObject>
- (void) backFromDetail;
@end

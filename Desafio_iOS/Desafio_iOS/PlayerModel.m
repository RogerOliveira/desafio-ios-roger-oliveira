//
//  PlayerModel.m
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/24/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import "PlayerModel.h"

@implementation PlayerModel
@synthesize name, photoURL;

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"name" : @"name",
             @"photoURL" : @"avatar_url"
             };
}

@end

//
//  FeedModel.h
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/25/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "NSValueTransformer+MTLPredefinedTransformerAdditions.h"

#import "ShotModel.h"

@interface FeedModel  : MTLModel <MTLJSONSerializing>

@property (nonatomic, readwrite) int totalPages;
@property (nonatomic, strong) NSArray* shots;

@end

//
//  ViewController.m
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/24/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import "ViewController.h"

NSString * const CELL_IDENTIFIER = @"FeedCell";
int const PAGINATION_DISTANCE = -100;

@interface ViewController ()

@end

@implementation ViewController
@synthesize tbv, shots, isLoading, hasMore, refreshControl;

#pragma mark Init methods

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupTableView];
    [self setupRefreshControl];
    [self setupFeedLoader];
}

- (void) setupTableView {
    self.tbv.dataSource = self;
    self.tbv.delegate = self;
    self.tbv.rowHeight = 290;//UITableViewAutomaticDimension;
    self.tbv.estimatedRowHeight = 290;
}

- (void) setupRefreshControl {
    UIRefreshControl* r = [[UIRefreshControl alloc] init];
    [self setRefreshControl:r];
    self.refreshControl.tintColor = [UIColor lightGrayColor];
    [self.refreshControl addTarget:self action:@selector(reload) forControlEvents:UIControlEventValueChanged];
    [self.tbv insertSubview:refreshControl atIndex:0];
}

- (void) setupFeedLoader {
    self.hasMore = YES;
    self.isLoading = YES;
    
    [FeedLoader getInstance].delegate = self;
    [self reload];
}

#pragma mark FeedLoader handling

- (void) nextPage {
    self.isLoading = YES;
    [[FeedLoader getInstance] loadNext];
}

- (void) reload {
    self.isLoading = YES;
    [[FeedLoader getInstance] reload];
}

- (void) feedLoaded:(NSArray*)_shots totalPages:(int)_totalPages currentPage:(int)_currentPage {
    
    NSLog(@"Page loaded: %i", _currentPage);
    
    self.isLoading = NO;
    [self.refreshControl endRefreshing];
    self.hasMore = _currentPage < _totalPages;
    [self setShots:[NSArray arrayWithArray:_shots]];
    
    [self.tbv reloadData];
}

- (void) feedLoaderFailed {
    self.isLoading = NO;
    [self showAlertError];
}

#pragma mark TableView handling
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return shots? [shots count] : 0;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    FeedCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    ShotModel* currentShot = [shots objectAtIndex:[indexPath row]];
    [cell setTitle:[currentShot title] imageURL:[currentShot image_url]];
    return cell;
}


- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    [self checkPagination];
}

- (void) checkPagination{
    if(hasMore && !isLoading) {
        CGPoint offset = self.tbv.contentOffset;
        CGRect bounds = self.tbv.bounds;
        CGSize size = self.tbv.contentSize;
        UIEdgeInsets inset = self.tbv.contentInset;
        float y = offset.y + bounds.size.height - inset.bottom;
        float h = size.height;
     

        if(y > h + PAGINATION_DISTANCE) {
            [self nextPage];
        }
    }
}

#pragma mark Detail handling

/*
 iOS 8 Bug here: https://forums.developer.apple.com/thread/5861
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetail"]) {

        DetailViewController* destViewController = segue.destinationViewController;
        destViewController.delegate = self;
        
        NSIndexPath* indexPath = [self.tbv indexPathForSelectedRow];
        ShotModel* currentShot = [shots objectAtIndex:[indexPath row]];
        [destViewController setupData:currentShot];
    }
}

- (void) backFromDetail {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark AlertView handling
- (void) showAlertError {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection failure"
                                                    message:@"Could not load the feed. Would you like to try it again?"
                                                   delegate:self cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [[FeedLoader getInstance] reload];
    }
}

@end

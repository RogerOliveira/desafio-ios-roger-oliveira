//
//  FeedCell.m
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/26/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import "FeedCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation FeedCell
@synthesize title, shot_image;

- (void) setTitle: (NSString*) _title imageURL:(NSString*) imageURL {
    
    [shot_image sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"loading_feed"]];
    
    title.text = _title;
}

@end

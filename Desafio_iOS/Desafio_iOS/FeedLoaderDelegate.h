//
//  FeedLoaderDelegate.h
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/24/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FeedLoaderDelegate <NSObject>
- (void) feedLoaded:(NSArray*)_shots totalPages:(int)_totalPages currentPage:(int)_currentPage;
- (void) feedLoaderFailed;
@end

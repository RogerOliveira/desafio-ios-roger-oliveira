//
//  DetailViewController.m
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/26/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation DetailViewController
@synthesize delegate, shot_image, shot_title, user_image, user_name, shot_description, currentShot;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(currentShot) {
        
        shot_title.text = [self.currentShot title];
        [self.shot_image sd_setImageWithURL:[NSURL URLWithString:[currentShot image_url]] placeholderImage:[UIImage imageNamed:@"loading_feed"]];
        
        [shot_description loadHTMLString:[currentShot shotDescription] baseURL:nil];
        
        if(currentShot.player) {
            
            [user_image sd_setImageWithURL:[NSURL URLWithString:currentShot.player.photoURL] placeholderImage:[UIImage imageNamed:@"loading_feed"]];

            [self makeMask:user_image maskFileName:@"mask"];
            
            user_name.text = [[currentShot player] name];
        }
    }
}


- (void) setupData:(ShotModel*) shot {
    [self setCurrentShot:shot];
}

- (IBAction) backPressed {
    [delegate backFromDetail];
    delegate = nil;
}

- (void) makeMask:(UIImageView*)_imageView maskFileName:(NSString*)_mask {
    UIImage* maskImage = [UIImage imageNamed:_mask];
    CALayer* maskLayer = [CALayer layer];
    maskLayer.frame = _imageView.bounds;
    [maskLayer setContents:(id)[maskImage CGImage]];
    [_imageView.layer setMask:maskLayer];
}


@end

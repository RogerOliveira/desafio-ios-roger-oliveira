//
//  FeedCell.h
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/26/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <SDWebImage/UIImageView+WebCache.h>

@interface FeedCell : UITableViewCell

- (void) setTitle: (NSString*) _title imageURL:(NSString*) imageURL;

@property (nonatomic, strong) IBOutlet UILabel* title;
@property (nonatomic, strong) IBOutlet UIImageView* shot_image;
@end

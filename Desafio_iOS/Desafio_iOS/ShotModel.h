//
//  ShotModel.h
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/24/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "NSValueTransformer+MTLPredefinedTransformerAdditions.h"

#import "PlayerModel.h"

@interface ShotModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* shotDescription;
@property (nonatomic, strong) NSString* image_url;
//@property (nonatomic, strong) NSArray* player;
@property (nonatomic, strong) PlayerModel* player;
@end

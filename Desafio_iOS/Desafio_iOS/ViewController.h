//
//  ViewController.h
//  Desafio_iOS
//
//  Created by Roger Oliveira on 9/24/15.
//  Copyright © 2015 Roger Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedLoader.h"
#import "FeedCell.h"
#import "DetailViewController.h"

extern NSString* const CELL_IDENTIFIER;
extern int const PAGINATION_DISTANCE;

@interface ViewController : UIViewController <FeedLoaderDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, DetailViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UITableView* tbv;
@property (nonatomic, strong) NSArray* shots;
@property (nonatomic, readwrite) BOOL isLoading;
@property (nonatomic, readwrite) BOOL hasMore;
@property (nonatomic, strong) UIRefreshControl* refreshControl;
@end

